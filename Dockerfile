FROM eclipse-temurin:11

COPY target/esercizio1-0.0.1-SNAPSHOT.jar /esercizio1-0.0.1-SNAPSHOT.jar
CMD ["java","-jar","esercizio1-0.0.1-SNAPSHOT.jar"]