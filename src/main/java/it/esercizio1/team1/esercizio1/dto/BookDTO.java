package it.esercizio1.team1.esercizio1.dto;

import lombok.Builder;

@Builder
public class BookDTO {
	
	private	String title;
	private int quantityOrdered;
	
	public BookDTO(String title, int quantityOrdered) {
		this.title = title;
		this.quantityOrdered = quantityOrdered;
	}

	public BookDTO() {
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getQuantityOrdered() {
		return quantityOrdered;
	}

	public void setQuantityOrdered(int quantityOrdered) {
		this.quantityOrdered = quantityOrdered;
	}

	@Override
	public String toString() {
		return "BookDTO [title=" + title + ", quantityOrdered=" + quantityOrdered + "]";
	}
	
	

}
