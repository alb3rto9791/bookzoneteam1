package it.esercizio1.team1.esercizio1.dto;

public class OrderUserDTO {

	private String title;
	
	private String nameUser;

	public OrderUserDTO(String title, String nameUser) {
		super();
		this.title = title;
		this.nameUser = nameUser;
	}

	public OrderUserDTO() {
		super();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getNameUser() {
		return nameUser;
	}

	public void setNameUser(String nameUser) {
		this.nameUser = nameUser;
	}

	@Override
	public String toString() {
		return "OrderUserDTO [title=" + title + ", nameUser=" + nameUser + "]";
	}
	
	
}
