package it.esercizio1.team1.esercizio1.dto;


public class OrderDTO {
	
	private String title;
	private int quantityOrdered;
	
	public OrderDTO() {
	}
	
	public OrderDTO(String title, int quantityOrdered) {
		this.title = title;
		this.quantityOrdered = quantityOrdered;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getQuantityOrdered() {
		return quantityOrdered;
	}
	public void setQuantityOrdered(int quantityOrdered) {
		this.quantityOrdered = quantityOrdered;
	}

	@Override
	public String toString() {
		return "OrderDTO [title=" + title + ", quantityOrdered=" + quantityOrdered + "]";
	}
	
	
}
