package it.esercizio1.team1.esercizio1.restController;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

import it.esercizio1.team1.esercizio1.dto.OrderUserDTO;
import it.esercizio1.team1.esercizio1.entity.User;
import it.esercizio1.team1.esercizio1.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserService userServ;

	Logger logger = LoggerFactory.getLogger(UserController.class);
	@PostMapping("/create")
	public ResponseEntity<?> create(@RequestBody User user) {
		logger.info("Sono nella classe UserController e sto eseguendo il metodo create");
		try {
			return ResponseEntity.ok()
					.body(userServ.create(user));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
		}	
	}
	
	@PutMapping("/setBalance/{id}")
	public ResponseEntity<?> setBalance(@PathVariable int id, @RequestParam ("balance") BigDecimal balance) throws SQLException {
		logger.info("Sono nella classe UserController e sto eseguendo il metodo setBalance");
		try {
			return ResponseEntity.ok()
					.body(userServ.setBalance(id, balance));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping("/setWishlist/{id}")
	public ResponseEntity<?> setWishlist(@PathVariable int id, @RequestParam ("title") String title) throws SQLException {
		logger.info("Sono nella classe UserController e sto eseguendo il metodo setWishlist");
		try {
			return ResponseEntity.ok()
					.body(userServ.setWishlist(id, title));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping("/buyBook/{id}")
	public ResponseEntity<?> buyBook(@PathVariable int id, @RequestParam ("title") String title) throws SQLException {
		logger.info("Sono nella classe UserController e sto eseguendo il metodo buyBook");
		try {
			User user = userServ.buyBook(id, title);
			return ResponseEntity.ok()
					.body(user);			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("/getWishlist/{id}")
	public ResponseEntity<?> getWishlist(@PathVariable int id) throws SQLException{
		logger.info("Sono nella classe UserController e sto eseguendo il metodo getWishlist");
		try {
			return ResponseEntity.ok()
					.body(userServ.getWishlist(id));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("/all")
	public List<User> all(){
		logger.info("Sono nella classe UserController e sto eseguendo il metodo all");
		return userServ.all();
	}
		
	@PostMapping("/asynBuyBook")
	public ResponseEntity<String> asyncBuyBook(@RequestBody OrderUserDTO orderUserDTO) throws InterruptedException, ExecutionException, JsonProcessingException{
		try {
			return userServ.asyncBuyBook(orderUserDTO);
		} catch (SQLException e) {	
			return ResponseEntity.internalServerError().body(e.getMessage());
		}
	}
}
