package it.esercizio1.team1.esercizio1.restController;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.esercizio1.team1.esercizio1.dto.OrderDTO;
import it.esercizio1.team1.esercizio1.entity.Book;
import it.esercizio1.team1.esercizio1.service.BookService;


@RestController
@RequestMapping("/books")
public class BookController {
	
	@Autowired
	BookService bookServ;
	
	Logger logger = LoggerFactory.getLogger(BookController.class);
	
	@PostMapping("/create")
	public Book create(@RequestBody Book book) {
		logger.info("Sono nella classe BookController e sto eseguendo il metodo create");
		return bookServ.create(book);
	}
	
	@GetMapping("/getAll")
	public List<Book> getAll() {
		logger.info("Sono nella classe BookController e sto eseguendo il metodo getAll");
		return bookServ.read();
	}
	
	@GetMapping("/get/{id}")
	public ResponseEntity<?> read(@PathVariable int id) {
		logger.info("Sono nella classe BookController e sto eseguendo il metodo read");
		try {
			return ResponseEntity.ok()
					.body(bookServ.read(id));
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			return new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
		}

	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<?> update(@PathVariable int id, @RequestBody Book book) {
		logger.info("Sono nella classe BookController e sto eseguendo il metodo update");
		try {
			return ResponseEntity.ok()
					.body(bookServ.update(id, book));
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			return new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
		}
		
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> delete(@PathVariable int id) {
		logger.info("Sono nella classe BookController e sto eseguendo il metodo delete");
		try {
			return ResponseEntity.ok()
					.body(bookServ.delete(id));
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			return new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
		}

	}
	@GetMapping("/restock")
	public ResponseEntity<?> restock(@RequestBody OrderDTO orderDTO){
		try {
			return ResponseEntity.ok().body(bookServ.restock(orderDTO)); 
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
	}
	}
	
	
}
