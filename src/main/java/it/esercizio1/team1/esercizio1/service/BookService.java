package it.esercizio1.team1.esercizio1.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import it.esercizio1.team1.esercizio1.dto.OrderDTO;
import it.esercizio1.team1.esercizio1.entity.Book;
import it.esercizio1.team1.esercizio1.repository.BookRepository;

@Service
public class BookService {

	@Autowired 
	BookRepository bookRepository;
	

	public Book create(Book book) {
		book.setCategory(book.getCategory().toUpperCase());
		bookRepository.save(book);
		return book;
	}
	

	public List<Book> read() {
		return bookRepository.findAll();
	}
	

	public Book read(int id) {
		Optional<Book> bookOp = bookRepository.findById(id);
		if(bookOp.isPresent()) {
			return bookOp.get();
		} else {
			throw new RuntimeException("Libro non trovato");
		}
	}
	

	public Book update(int id, Book book) {
		Optional<Book> bookOp = bookRepository.findById(id);
		Book book1;
		if(bookOp.isPresent()) {
			book1 = bookOp.get();
		} else {
			throw new RuntimeException("Libro non trovato");
		}
		book1.setAuthor(book.getAuthor());
		book1.setTitle(book.getTitle());
		book1.setPrice(book.getPrice());
		book1.setStock(book.getStock());
		return bookRepository.save(book1);
		
	}
	

	public String delete(int id) {
		Optional<Book> bookOp = bookRepository.findById(id);
		if(bookOp.isPresent()) {
			bookRepository.deleteById(id);
			return "Libro eliminato " + id;
		} else {
			throw new RuntimeException("Libro non trovato");
		}
	}
	
	public Book restock(OrderDTO orderDTO) {
		Optional<Book> bookOp = bookRepository.findByTitle(orderDTO.getTitle());
		Book book;
		if(bookOp.isPresent()) {
			book = bookOp.get();
		} else {
			throw new RuntimeException("Libro non trovato");
		}
		System.out.println(orderDTO);
		RestTemplate restTemp = new RestTemplate();
		OrderDTO orderDTO2 = restTemp.postForObject("http://localhost:8081/order/create", orderDTO , OrderDTO.class);
		book.setStock(orderDTO2.getQuantityOrdered()+book.getStock());
		return bookRepository.save(book);
	} 

	
}
