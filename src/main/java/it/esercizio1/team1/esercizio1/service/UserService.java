package it.esercizio1.team1.esercizio1.service;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.esercizio1.team1.esercizio1.dto.OrderUserDTO;
import it.esercizio1.team1.esercizio1.entity.Book;
import it.esercizio1.team1.esercizio1.entity.User;
import it.esercizio1.team1.esercizio1.repository.BookRepository;
import it.esercizio1.team1.esercizio1.repository.UserRepository;

@Service
public class UserService {
	
	
	private final String topic;
	
	public UserService(@Value("${messaging.output.topic}")String topic) {
		this.topic = topic;
	}

	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;
	
	@Autowired
	private UserRepository userRep;

	@Autowired
	private BookRepository bookRep;


	public User create(User user) throws DataIntegrityViolationException {
		try {
			user.setBalance(new BigDecimal(0.00));
			return userRep.save(user);
		} catch (Exception e) {
			throw new DataIntegrityViolationException("Data e nome richiesti");
		}
	}

	public User setBalance(int id, BigDecimal balance) throws SQLException {
		Optional<User> user = userRep.findById(id);
		if (user.isPresent()) {
			User user1 = user.get();
			user1.setBalance(balance);
			return userRep.save(user1);
		} else {
			throw new SQLException("utente non trovato con id - " + id);
		}
	}

	public User setWishlist(int id, String title) throws SQLException {
		System.out.println(title);
		Optional<User> user = userRep.findById(id);
		if (!user.isPresent()) {
			throw new SQLException("utente non trovato con id - " + id);
		}
		Optional<Book> book = bookRep.findByTitle(title);
		if (!book.isPresent()) {
			throw new SQLException("libro con titolo " + title + " non trovato");
		}
		User user1 = user.get();
		user1.getBooks().add(book.get());
		return userRep.save(user1);
	}

	public User buyBook(int id, String title) throws SQLException {
		Optional<User> userOp = userRep.findById(id);
		if (!userOp.isPresent()) {
			throw new SQLException("null");
		}
		Optional<Book> bookOp = bookRep.findByTitle(title);
		if (!bookOp.isPresent()) {
			throw new SQLException("Libro non presente");
		}
		Book book = bookOp.get();
		User user = userOp.get();
		LocalDate dateNow = LocalDate.now();
		if (dateNow.getYear() - user.getDate().toLocalDate().getYear() < 18) {
			if (book.getCategory().equals("HORROR") || book.getCategory().equals("EROTICO"))
				throw new RuntimeException("Non puoi comprare questo libro");
		}
		if (book.getStock() > 0) {
			if (user.getBalance().compareTo(book.getPrice()) >= 0) {
				user.setBalance(user.getBalance().subtract(book.getPrice()));
				book.setStock(book.getStock() - 1);
				bookRep.save(book);
				List<Book> listBooks = user.getBooks();
				listBooks = listBooks.stream().filter(bookInList->!bookInList.getTitle().equals(book.getTitle())).collect(Collectors.toList());
/*				for (int i = 0; i < listBooks.size(); i++) {
					Book book2 = listBooks.get(i);
					if (book2.getTitle().equals(book.getTitle())) {
						listBooks.remove(i);
					}
				} */
				user.setBooks(listBooks);
				userRep.save(user);
				return user;
			}
			throw new RuntimeException("Saldo non sufficiente");
		}
		throw new RuntimeException("Libro non presente");
	}

	public List<Book> getWishlist(int id) throws SQLException {
		Optional<User> user = userRep.findById(id);
		if (!user.isPresent()) {
			throw new SQLException("utente non trovato con id - " + id);
		}
		return user.get().getBooks();
	}

	public List<User> all() {
		return userRep.findAll();
	}
	
	public ResponseEntity<String> asyncBuyBook(OrderUserDTO orderUserDTO) throws SQLException, InterruptedException, ExecutionException, JsonProcessingException {
		Optional<User> userOp = userRep.findByName(orderUserDTO.getNameUser());
		if (!userOp.isPresent()) {
			throw new SQLException("utente non trovato");
		}
		Optional<Book> bookOp = bookRep.findByTitle(orderUserDTO.getTitle());
		if (!bookOp.isPresent()) {
			throw new SQLException("libro non trovato");
		}
		Book book = bookOp.get();
		if(book.getStock()>0) {
			return ResponseEntity.badRequest().body("KO");
		}
		ObjectMapper objMap = new ObjectMapper();
		kafkaTemplate.send(topic, orderUserDTO.getNameUser(), objMap.writeValueAsString(orderUserDTO)).get();
		return ResponseEntity.ok().body("OK");
		
	}
	@KafkaListener(id = "#{'${spring.application.name}'}", topics = "#{'${messaging.input.topic}'.split(',')}", containerFactory = "kafkaListenerContainerFactory")
	public void setOrdine(String orderValidate) throws SQLException {
		Gson gson = new Gson();
		OrderUserDTO orderUserDTO = gson.fromJson(orderValidate,OrderUserDTO.class);
		Optional<User> userOp = userRep.findByName(orderUserDTO.getNameUser());
		if(!userOp.isPresent()){
			throw new SQLException("Utente non presente");
		}
		Optional<Book> bookOp = bookRep.findByTitle(orderUserDTO.getTitle());
		if(!bookOp.isPresent()){
			throw new SQLException("Libro non presente");
		}
		User user = userOp.get();
		Book book = bookOp.get();

		user.setToRetire(book);
		userRep.save(user);
	}
}
