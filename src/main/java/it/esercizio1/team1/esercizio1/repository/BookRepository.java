package it.esercizio1.team1.esercizio1.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import it.esercizio1.team1.esercizio1.entity.Book;

public interface BookRepository extends JpaRepository<Book, Integer>{
	Optional<Book> findByTitle(String title);
}
