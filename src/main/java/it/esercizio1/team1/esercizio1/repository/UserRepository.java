package it.esercizio1.team1.esercizio1.repository;

import java.sql.Date;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import it.esercizio1.team1.esercizio1.entity.User;

public interface UserRepository extends JpaRepository<User, Integer>{
	Optional<User> findByNameAndDate(String name, Date data);
	Optional<User> findByName(String name);
}
