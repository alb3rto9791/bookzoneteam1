package it.esercizio1.team1.esercizio1.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.Nullable;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class User {
	@Id
	@GeneratedValue(strategy =  GenerationType.IDENTITY)
	private int id;

	@Column(name = "name")
	private String name;
	
	@Column(name = "date")
	private Date date;
	
	@Column(name = "balance")
	private BigDecimal balance;
	
	@Column(name="address")
	private String address;


	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
			name = "users_books",
			joinColumns = @JoinColumn(name="user_id",referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name="book_id",referencedColumnName = "id")
			)
	private List<Book> books;

	@LazyCollection(LazyCollectionOption.FALSE)
	@ManyToMany()
	@JoinTable(
			name = "to_retire",
			joinColumns = @JoinColumn(name="user_id",referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name="book_id",referencedColumnName = "id")
	)
	@Nullable
	private List<Book> toRetire;

	public List<Book> getToRetire() {
		return toRetire;
	}

	public void setToRetire(Book toRetire) {
		this.toRetire.add(toRetire);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}

	public User(String name, Date date, BigDecimal balance, String address, List<Book> books) {
		this.name = name;
		this.date = date;
		this.balance = balance;
		this.address = address;
		this.books = books;
	}

	public User() {
	}

	@Override
	public String toString() {
		return "User [userId=" + id + ", name=" + name + ", date=" + date + ", balance=" + balance + ", address="
				+ address + ", books=" + books + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(address, balance, books, date, name, id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		return Objects.equals(address, other.address) && Objects.equals(balance, other.balance)
				&& Objects.equals(books, other.books) && Objects.equals(date, other.date)
				&& Objects.equals(name, other.name) && id == other.id;
	}

	public int getUserId() {
		return id;
	}

	




}
