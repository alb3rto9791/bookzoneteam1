CREATE DATABASE bookzone;
CREATE TABLE bookzone.books (
	id INTEGER AUTO_INCREMENT PRIMARY KEY,
	title VARCHAR(50) NOT NULL,
    author VARCHAR(50) NOT NULL,
    price DECIMAL NOT NULL,
    stock INTEGER DEFAULT(0)
);
CREATE TABLE bookzone.users (
 id INTEGER AUTO_INCREMENT PRIMARY KEY,
 `name` VARCHAR(100) NOT NULL,
 `date` DATE NOT NULL,
 balance DECIMAL DEFAULT(0),
 address VARCHAR(50),
 wishlist ??? ,
 FOREIGN KEY(wishlist) REFERENCES bookzone.books(id)
);